<?php

namespace Doblhofer;

class HelloWorld {
    public function sayHello() {
        return 'Hello World!';
    }
}
