# Demo Repository for a custom PHP Composer package on GitLab with PHPUnit Tests

More details can be found in the following blog post:

[Custom PHP Composer Package on GitLab with PHPUnit Tests (English)](https://www.philipp-doblhofer.at/en/blog/custom-php-composer-package-on-gitlab-with-phpunit-tests/)\
[Eigenes PHP Composer Paket in GitLab mit PHPUnit Tests (Deutsch)](https://www.philipp-doblhofer.at/blog/eigenes-php-composer-paket-in-gitlab-mit-phpunit-tests/)
